using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance = null;

    private const float width = 3.6f;
    private const float height = 7f;

    public float snakeSpeed = 1;

    public BodyPart bodyPrefab = null;
    public GameObject rockPrefab = null;
    public GameObject eggPrefab = null;
    public GameObject goldenEggPrefab = null;
    public GameObject spikePrefab = null;

    public Sprite bodySprite = null;
    public Sprite tailSprite = null;

    public SnakeHead snakeHead = null;

    public bool alive = true;

    public bool waitingToPlay = true;

    private List<Egg> eggs = new List<Egg>();
    private List<Spike> spikes = new List<Spike>();

    private int level = 0;
    private int numberOfEggsForNextLevel = 0;

    public int score = 0;
    public int highScore = 0;

    public TMP_Text scoreText = null;
    public TMP_Text highScoreText = null;

    public TMP_Text gameOverText = null;
    public TMP_Text tapToPlayText = null;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Debug.Log("Starting Snake Game");
        CreateWalls();
        alive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (waitingToPlay)
        {
            foreach (var touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Ended)
                {
                    StartGamePlay();
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                StartGamePlay();
            }
        }
    }

    public void GameOver()
    {
        alive = false;
        waitingToPlay = true;

        gameOverText.gameObject.SetActive(true);
        tapToPlayText.gameObject.SetActive(true);
    }

    private void StartGamePlay()
    {
        score = 0;
        level = 0;

        scoreText.text = "Score: " + score;
        highScoreText.text = "Hi Score: " + highScore;

        gameOverText.gameObject.SetActive(false);
        tapToPlayText.gameObject.SetActive(false);

        waitingToPlay = false;
        alive = true;

        KillOldEggs();
        KillOldSpikes();

        LevelUp();
    }

    private void LevelUp()
    {
        level++;

        numberOfEggsForNextLevel = 4 + (level * 2);
        snakeSpeed = 1f + (level / 4f);
        
        if (snakeSpeed > 6)
        {
            snakeSpeed = 6;
        }

        snakeHead.ResetSnake();
        CreateEgg();

        CreateSpike();
    }

    public void EggEaten(Egg egg)
    {
        score++;
        scoreText.text = "Score: " + score;

        numberOfEggsForNextLevel--;
        if (numberOfEggsForNextLevel == 0)
        {
            score += 10;
            scoreText.text = "Score: " + score;
            LevelUp();
        }
        else if (numberOfEggsForNextLevel == 1)
        {
            CreateEgg(true);
        }
        else
        {
            CreateEgg(false);
        }

        if (score > highScore)
        {
            highScore = score;
            highScoreText.text = "Hi Score: " + highScore;
        }

        eggs.Remove(egg);
        Destroy(egg.gameObject);
    }

    private void CreateWalls()
    {
        var z = -0.1f;

        var start = new Vector3(-width, -height, z);
        var finish = new Vector3(-width, height, z);
        CreateWall(start, finish);

        start = new Vector3(width, -height, z);
        finish = new Vector3(width, height, z);
        CreateWall(start, finish);

        start = new Vector3(-width, -height, z);
        finish = new Vector3(width, -height, z);
        CreateWall(start, finish);

        start = new Vector3(-width, height, z);
        finish = new Vector3(width, height, z);
        CreateWall(start, finish);
    }

    private void CreateWall(Vector3 start, Vector3 finish)
    {
        var distance = Vector3.Distance(start, finish);
        var numberOfRocks = (int)(distance * 3f);
        var delta = (finish - start)/numberOfRocks;

        var position = start;

        for (var rock = 0; rock <= numberOfRocks; rock++)
        {
            var rotation = Random.Range(0f, 360f);
            var scale = Random.Range(1.5f, 2f);
            CreateRock(position, scale, rotation);
            position = position + delta;
        }
    }

    private void CreateRock(Vector3 position, float scale, float rotation)
    {
        var rock = Instantiate(rockPrefab, position, Quaternion.Euler(0, 0, rotation));
        rock.transform.localScale = new Vector3(scale, scale, 1);
    }

    private void CreateEgg(bool goldenEgg = false)
    {
        Vector3 position;
        position.x = -width + Random.Range(1f, (width * 2) - 2f);
        position.y = -height + Random.Range(1f, (height * 2) - 2f);
        position.z = -0.1f;
        Egg egg = null;
        if (goldenEgg)
        {
            egg = Instantiate(goldenEggPrefab, position, Quaternion.identity).GetComponent<Egg>();
        }
        else
        {
            egg = Instantiate(eggPrefab, position, Quaternion.identity).GetComponent<Egg>();
        }

        eggs.Add(egg);
    }

    private void CreateSpike()
    {
        Vector3 position;
        position.x = -width + Random.Range(1f, (width * 2) - 2f);
        position.y = -height + Random.Range(1f, (height * 2) - 2f);
        position.z = -0.1f;
        Spike spike = Instantiate(spikePrefab, position, Quaternion.identity).GetComponent<Spike>();

        spikes.Add(spike);
    }

    private void KillOldEggs()
    {
        foreach (var egg in eggs)
        {
            Destroy(egg.gameObject);
        }

        eggs.Clear();
    }

    private void KillOldSpikes()
    {
        foreach (var spike in spikes)
        {
            Destroy(spike.gameObject);
        }

        spikes.Clear();
    }
}
