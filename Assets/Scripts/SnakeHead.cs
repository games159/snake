using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeHead : BodyPart
{
    private Vector2 movement;

    private BodyPart tail = null;

    const float TIMETOADDBODYPART = 0f;
    float addTimer = TIMETOADDBODYPART;

    public int partsToAdd = 0;

    private List<BodyPart> parts = new List<BodyPart>();

    public AudioSource[] gulpSounds = new AudioSource[3];
    public AudioSource dieSound = null;

    // Start is called before the first frame update
    void Start()
    {
        SwipeControls.OnSwipe += SwipeDetection;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (!GameController.instance.alive)
        {
            return;
        }

        base.Update();
        SetMovement(movement * Time.deltaTime);
        UpdateDirection();
        UpdatePosition();

        if (partsToAdd > 0)
        {
            addTimer -= Time.deltaTime;
            if (addTimer <= 0)
            {
                addTimer = TIMETOADDBODYPART;
                AddBodyPart();
                partsToAdd--;
            }
        }
    }

    private void AddBodyPart()
    {
        if (tail == null)
        {
            Vector3 newPosition = transform.position;
            newPosition.z = newPosition.z + 0.01f;

            var newPart = Instantiate(GameController.instance.bodyPrefab, newPosition, Quaternion.identity);
            newPart.following = this;
            tail = newPart;
            newPart.TurnIntoTail();

            parts.Add(newPart);
        }
        else
        {
            var newPosition = tail.transform.position;
            newPosition.z = newPosition.z + 0.01f;

            var newPart = Instantiate(GameController.instance.bodyPrefab, newPosition, tail.transform.rotation);
            newPart.following = tail;
            newPart.TurnIntoTail();
            tail.TurnIntoBodyPart();
            tail = newPart;

            parts.Add(newPart);
        }
    }

    private void SwipeDetection(SwipeControls.SwipeDirection direction)
    {
        switch (direction)
        {
            case SwipeControls.SwipeDirection.Up:
                MoveUp();
                break;
            case SwipeControls.SwipeDirection.Down:
                MoveDown();
                break;
            case SwipeControls.SwipeDirection.Left:
                MoveLeft();
                break;
            case SwipeControls.SwipeDirection.Right:
                MoveRight();
                break;
        }
    }

    private void MoveUp()
    {
        movement = GameController.instance.snakeSpeed * Vector2.up;
    }

    private void MoveDown()
    {
        movement = GameController.instance.snakeSpeed * Vector2.down;
    }

    private void MoveLeft()
    {
        movement = GameController.instance.snakeSpeed * Vector2.left;
    }

    private void MoveRight()
    {
        movement = GameController.instance.snakeSpeed * Vector2.right;
    }

    public void ResetSnake()
    {
        foreach (var part in parts)
        {
            Destroy(part.gameObject);
        }

        parts.Clear();

        tail = null;
        MoveUp();

        gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
        gameObject.transform.position = new Vector3(0, 0, -8f);

        ResetMemory();

        partsToAdd = 5;
        addTimer = TIMETOADDBODYPART;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var egg = collision.GetComponent<Egg>();

        if (egg)
        {
            EatEgg(egg);
            var rand = Random.Range(0, 3);
            gulpSounds[rand].Play();
        }
        else
        {
            GameController.instance.GameOver();
            dieSound.Play();
        }
    }

    private void EatEgg(Egg egg)
    {
        partsToAdd = 5;
        addTimer = 0;

        GameController.instance.EggEaten(egg);
    }
}
